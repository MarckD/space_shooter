﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public float velocity;
    public Vector2 direction;

    private void Update()
    {
        transform.Translate(direction * velocity * Time.deltaTime);
    }

   public void OnTriggerEnter2D(Collider2D other)
   {
      if (other.tag == "Finish" || other.tag == "Meteor")
      {
          Destroy(gameObject);
      }
   }
}